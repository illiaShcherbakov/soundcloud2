import React from 'react';
import { connect } from 'react-redux';
import { debounce } from '../../utils/helpers';
import Song from '../../components/Song/Song';
import Player from '../../components/Player/Player';
import {
  middlewareGetSogns,
  middlewareAddSongs,
  setCurrentSong
} from './songs-actions';
import './songs.scss';

class Songs extends React.Component {
  state = {
    showSongs: false
  };

  componentDidMount() {
    const db_moar = debounce(
      nextHref => this.props.middlewareAddSongs(nextHref),
      700,
      true
    );
    const appContainer = document.getElementById('app-container');
    window.addEventListener(
      'scroll',
      () => {
        if (
          window.innerHeight + window.scrollY >=
          document.body.offsetHeight - 200
        ) {
          db_moar(this.props.nextHref);
          appContainer.scrollTop = appContainer.scrollTop + 70;
        }
      },
      false
    );
  }

  componentWillMount = () => {
    const genre = this.props.match.params.genre
      ? this.props.match.params.genre
      : this.props.firstLoadGenre;

    this.props.middlewareGetSogns(genre);
  };

  componentWillReceiveProps = nextProps => {
    if (this.props.match.params.genre !== nextProps.match.params.genre) {
      this.props.middlewareGetSogns(nextProps.match.params.genre);
    }
  };

  renderSongsList() {
    const { songs, currentSong } = this.props;

    if (songs) {
      return songs.map(item => {
        return (
          item && (
            <Song
              key={item.id}
              id={item.id}
              artwork={item.artwork_url}
              title={item.title}
              onClick={() => this.props.setCurrentSong(item.id)}
              isActive={currentSong === item.id}
            />
          )
        );
      });
    }
    return [];
  }

  render() {
    let classes = 'songs';
    classes = this.state.showSongs ? `${classes} active` : classes;

    return (
      <div className={classes}>
        {this.renderSongsList()}
        {!!this.props.currentSong && (
          <Player
            id="player"
            className="player"
            songsData={this.props.songs}
            streamSongId={this.props.currentSong}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    songs: state.songs.songs,
    firstLoadGenre: state.menu.activeMenuItem,
    currentSong: state.songs.currentSong
  };
};

const mapDispatchToProps = {
  middlewareAddSongs,
  middlewareGetSogns,
  setCurrentSong
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Songs);
