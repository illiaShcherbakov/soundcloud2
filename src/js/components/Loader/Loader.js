import React from 'react';
import './loader.scss';

const Loader = props => {
  let classes = 'cs-loader';
  classes = props.isVisible ? (classes += ' active') : classes;

  return (
    <div className={classes}>
      <div className="cs-loader-inner">
        <label> ●</label>
        <label> ●</label>
        <label> ●</label>
        <label> ●</label>
        <label> ●</label>
        <label> ●</label>
      </div>
    </div>
  );
};

export default Loader;
