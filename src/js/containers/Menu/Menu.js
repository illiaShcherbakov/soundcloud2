import React from 'react';
import { connect } from 'react-redux';
import MenuItem from '../../components/MenuItem';
import './menu.scss';
import { activeMenu } from './menu-actions';

class Menu extends React.Component {
  state = {
    listOfGenres: [
      { name: 'Chill', isActive: true },
      { name: 'Deep', isActive: false },
      { name: 'Dubstep', isActive: false },
      { name: 'House', isActive: false },
      { name: 'Progressive', isActive: false },
      { name: 'Tech', isActive: false },
      { name: 'Trance', isActive: false },
      { name: 'Tropical', isActive: false }
    ]
  };

  componentWillMount = () => {
    const genre = this.props.match.params.genre
      ? this.props.match.params.genre
      : this.props.activeGenre;

    console.log(this.props.match.params.genre, genre);
    this.toggleClass(genre);
    this.props.activeMenu(genre);
  };

  toggleClass = name => {
    const newList = this.state.listOfGenres.map(item => {
      item.isActive = item.name === name;
      return item;
    });

    this.setState({
      listOfGenres: newList
    });
  };

  render() {
    return (
      <ul className="menu">
        {this.state.listOfGenres.map(item => {
          return (
            <MenuItem
              key={item.name.toString()}
              name={item.name}
              isActive={item.isActive}
              toggleClass={() => this.toggleClass(item.name)}
            />
          );
        })}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return {
    activeGenre: state.menu.activeMenuItem
  };
};

export default connect(
  mapStateToProps,
  { activeMenu }
)(Menu);
