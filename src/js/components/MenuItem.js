import React from 'react';
import { NavLink } from 'react-router-dom';

const MenuItem = ({ isActive, name, toggleClass }) => {
  return (
    <li className={isActive ? 'active' : undefined}>
      <NavLink to={`/${name}`} onClick={toggleClass}>
        {name}
      </NavLink>
    </li>
  );
};

export default MenuItem;
