export const actionTypes = {
  TOGGLE_LOADER: 'TOGGLE_LOADER',
  CLEAR: 'CLEAR',
  GET_FIRST_SONGS: 'GET_FIRST_SONGS',
  ADD_SONGS: 'ADD_SONGS',
  SET_CURRENT_SONG: 'SET_CURRENT_SONG'
};

function loader() {
  return {
    type: actionTypes.TOGGLE_LOADER
  };
}

function clearSongs() {
  return {
    type: actionTypes.CLEAR
  };
}

function getFistSogns(tracks, next_href) {
  return {
    type: actionTypes.GET_FIRST_SONGS,
    data: tracks,
    nextHref: next_href
  };
}

export function middlewareGetSogns(genre) {
  return dispatch => {
    dispatch(loader());
    dispatch(clearSongs());
    fetch(
      'http://api.soundcloud.com/tracks?&client_id=jCL7iQfm5WBlS94Fls1jtWetQxxi1xFd&genres=' +
        genre +
        '&limit=60&linked_partitioning=1'
    )
      .then(tracks => {
        const songs = tracks.json();
        return songs;
      })
      .then(songs => {
        dispatch(getFistSogns(songs.collection, songs.next_href));
        dispatch(loader());
      });
  };
}

function addSongs(newSongs, next_href) {
  return {
    type: actionTypes.ADD_SONGS,
    addedSongs: newSongs,
    nextHref: next_href
  };
}

export const setCurrentSong = songId => {
  return {
    type: actionTypes.SET_CURRENT_SONG,
    payload: songId
  };
};

export function middlewareAddSongs(href) {
  return dispatch => {
    dispatch(loader());
    fetch(href)
      .then(tracks => {
        const songs = tracks.json();
        return songs;
      })
      .then(songs => {
        setTimeout(() => {
          dispatch(addSongs(songs.collection, songs.next_href));
          dispatch(loader());
        }, 1000);
      });
  };
}
