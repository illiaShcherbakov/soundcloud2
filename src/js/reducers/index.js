import { combineReducers } from 'redux';
import { songs } from '../containers/Songs/songs-reducer';
import { menu } from '../containers/Menu/menu-reducer';

const rootReducer = combineReducers({
  songs,
  menu
});

export default rootReducer;
