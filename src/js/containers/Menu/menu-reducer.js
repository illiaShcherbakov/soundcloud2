import { actionTypes as menuActionTypes } from './menu-actions';

const initialState = {
  activeMenuItem: 'Chill'
};

export const menu = (state = initialState, action) => {
  switch (action.type) {
    case menuActionTypes.ACTIVE_MENU:
      return {
        ...state,
        activeMenuItem: action.activeGenre
          ? action.activeGenre
          : state.activeMenuItem
      };
    default:
      return state;
  }
};
