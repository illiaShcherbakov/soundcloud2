import { actionTypes as songsActionTypes } from './songs-actions';

const initialState = {
  songs: null,
  nextHref: null,
  loader: false,
  currentSong: null
};

export const songs = (state = initialState, action) => {
  switch (action.type) {
    case songsActionTypes.GET_FIRST_SONGS:
      return {
        ...state,
        songs: action.data,
        nextHref: action.nextHref
      };
    case songsActionTypes.ADD_SONGS:
      return {
        ...state,
        songs: [...state.songs, ...action.addedSongs],
        nextHref: action.nextHref
      };
    case songsActionTypes.TOGGLE_LOADER:
      return {
        ...state,
        loader: !state.loader
      };
    case songsActionTypes.CLEAR:
      return {
        ...state,
        songs: null
      };
    case songsActionTypes.SET_CURRENT_SONG:
      return {
        ...state,
        currentSong: action.payload
      };
    default:
      return state;
  }
};
