import React from 'react';
import './player.scss';

export default class Player extends React.Component {
  state = {
    playerInstance: null,
    playlist: null,
    isVisible: false,
    songData: {},
    prevDisable: false,
    nextDisable: false
  };
  player = null;

  componentWillMount = () => {
    this.setSongToPLay(this.props);
  };

  componentDidMount = () => {
    this.player = document.getElementById('player');
    this.player.volume = 0.2;
    this.player.autoplay = true;
  };

  componentWillReceiveProps = nextProps => {
    if (this.props.streamSongId !== nextProps.streamSongId) {
      this.setSongToPLay(nextProps);
      this.player.autoplay = true;
      this.player.pause();
    }
  };

  togglePlayerPlay = () => {
    switch (this.player.paused) {
      case true:
        this.player.play();
        break;
      default:
        this.player.pause();
        break;
    }
  };

  setSongToPLay = props => {
    const { songsData, streamSongId } = props;

    songsData.filter(item => item.id === streamSongId).map(item => {
      const songIndex = songsData.findIndex(x => x.id === item.id);
      this.setState({
        playlist: songsData,
        isVisible: true,
        songData: {
          number: songIndex,
          songToPlay: item
        },
        prevDisable: !songIndex,
        nextDisable: songIndex === songsData.length - 1
      });
    });
  };

  handlePrev = () => {
    const { songData, playlist } = this.state;

    const nextState = {
      songData: {
        number: songData.number - 1,
        songToPlay: playlist[songData.number - 1]
      },
      prevDisable: false,
      nextDisable: false
    };
    if (songData.number - 1 === 0) {
      nextState.prevDisable = true;
    }
    this.setState(nextState);
    this.player.autoplay = true;
  };

  handleNext = () => {
    const { songData, playlist } = this.state;
    let nextState = {
      songData: {
        number: songData.number + 1,
        songToPlay: playlist[songData.number + 1]
      }
    };
    if (songData.number + 1 >= playlist.length - 1) {
      nextState = {
        ...nextState,
        nextDisable: true,
        prevDisable: false
      };
    } else {
      nextState = {
        ...nextState,
        prevDisable: false
      };
    }

    this.setState(nextState);
    this.player.autoplay = true;
  };

  handleEnd = () => {
    this.handleNext();
  };

  render() {
    const { id, className } = this.props;
    const playerClass = this.state.isVisible
      ? `${className} active`
      : className;

    const [author, title] = this.state.songData.songToPlay.title.split(' - ');
    const url =
      this.state.songData.songToPlay.stream_url +
      '?&client_id=jCL7iQfm5WBlS94Fls1jtWetQxxi1xFd';
    return (
      <div className={playerClass}>
        <div className="player-meta">
          <span className="palyer-author">{title ? author : ''}</span>
          {title ? <span className="palyer-author"> - </span> : ''}
          <span className="player-title">{title ? title : author}</span>
        </div>
        <button
          className="player-prev"
          onClick={this.handlePrev}
          disabled={this.state.prevDisable}
        />
        <audio
          className="player-audio"
          id={id}
          src={url}
          controls
          onEnded={this.handleEnd}
        />
        <button
          className="player-next"
          onClick={this.handleNext}
          disabled={this.state.nextDisable}
        />
      </div>
    );
  }
}
