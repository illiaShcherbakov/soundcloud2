export const actionTypes = {
  ACTIVE_MENU: 'ACTIVE_MENU'
};

export function activeMenu(genre) {
  return {
    type: actionTypes.ACTIVE_MENU,
    activeGenre: genre
  };
}
