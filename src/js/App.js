import React from 'react';
import { connect } from 'react-redux';
import Menu from './containers/Menu/Menu';
import Loader from './components/Loader/Loader';
import Songs from './containers/Songs/Songs';

class App extends React.Component {
  state = {
    isLoading: true
  };

  render() {
    return (
      <div id="app-container">
        <Menu {...this.props} />
        <Songs {...this.props} />
        <Loader isVisible={this.props.loader} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    nextHref: state.songs.nextHref,
    loader: state.songs.loader
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
