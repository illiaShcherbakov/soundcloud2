import React from 'react';
import songPlaceholder from '../../../img/no_img.jpg';
import './song.scss';

export const Song = ({ title, isActive, artwork, onClick }) => {
  const [author, label] = title.split(' - ');

  let songClass = 'song';
  songClass = isActive ? `${songClass} ${songClass}--active` : songClass;

  return (
    <div className={songClass}>
      <img
        className=""
        src={artwork ? artwork : songPlaceholder}
        alt=""
        onClick={onClick}
      />

      <div className="metadata">
        {label && <p className="author">{author}</p>}
        <p className="title">{label ? label : author}</p>
      </div>
    </div>
  );
};

export default Song;
